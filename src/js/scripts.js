const loremIpsum = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
Aenean ac augue hendrerit ex dapibus suscipit eget sed velit. Integer id tortor lobortis, 
dapibus lectus ullamcorper, blandit metus.
<span class="points" style="display: inline;">...</span>
<span class="more-text" style="display:none;">
Maecenas rutrum sapien non purus fringilla vehicula. Nam sit amet ipsum ac mi bibendum porttitor. 
Curabitur id nunc molestie, suscipit sem eu, rutrum nunc. Quisque lacinia turpis neque, at rhoncus mi laoreet sit amet. 
Fusce interdum urna magna, ut auctor quam aliquam et. Morbi posuere faucibus turpis, ac maximus diam cursus condimentum.
Sed vulputate, odio vel mollis auctor, erat massa ornare mi, mollis rhoncus libero lectus sit amet nisi. 
Pellentesque accumsan, ipsum at tempor mattis, quam tortor placerat purus, eget elementum risus nibh vel purus. U
t id ex at magna molestie efficitur. Cras ligula nulla, volutpat accumsan dui eget, porta lobortis sem. Pellentesque feugiat egestas
lacus a vehicula. Phasellus cursus sapien ut est lobortis, quis maximus neque lacinia. Proin tristique posuere libero, vitae finibus
metus commodo a.<br>Donec ut enim lacus. Proin eu eleifend lorem, aliquet aliquet sem. Nullam vitae dolor non est sodales dapibus a in
dui. Curabitur volutpat nisl ac justo tristique molestie. Donec a fringilla tortor. Nulla non quam mauris. Phasellus luctus sodales
venenatis. Aliquam hendrerit luctus viverra. Mauris consectetur, sem nec pretium condimentum, ante ex scelerisque velit, vitae lacinia
justo erat at velit. Nulla eu mi eu odio varius lacinia id ac nulla. Aenean auctor nisl sed diam aliquam, vel accumsan massa scelerisque.
 Morbi sodales urna felis, molestie posuere nisl bibendum eu. Quisque dapibus placerat nisi, vel eleifend quam vehicula at. Aenean id eros</span>`
const articles = [
    {
        title: "Wonderful Copenhagen 2011",
        date: "2/12/2010",
        body: loremIpsum
    },
    {
        title: "Nordic barista cup in Copenhagen",
        date: "2/12/2011",
        body: loremIpsum
    },
    {
        title: "Who will be the next champion?",
        date: "4/12/2010",
        body: loremIpsum
    },
    {
        title: "Copenhagen ready to host the next championship event",
        date: "2/12/2010",
        body: loremIpsum
    },
    {
        title: "Article 5",
        date: "2/12/2010",
        body: loremIpsum
    },
    {
        title: "Article nr. 6",
        date: "2/12/2010",
        body: loremIpsum
    },
    {
        title: "Article nr. 7",
        date: "2/12/2010",
        body: loremIpsum
    },
    {
        title: "Article nr. 8",
        date: "2/12/2010",
        body: loremIpsum
    },
    {
        title: "Article nr. 9",
        date: "2/12/2010",
        body: loremIpsum
    },
    {
        title: "Article nr. 10",
        date: "2/12/2010",
        body: loremIpsum
    },
    {
        title: "Article nr. 11",
        date: "2/12/2010",
        body: loremIpsum
    },
    {
        title: "Article nr. 12",
        date: "2/12/2010",
        body: loremIpsum
    },
    {
        title: "Article nr. 13",
        date: "2/12/2010",
        body: loremIpsum
    },
    {
        title: "Article nr. 14",
        date: "2/12/2010",
        body: loremIpsum
    },
    {
        title: "Article nr. 15",
        date: "2/12/2010",
        body: loremIpsum
    }
]

const paginationWrapper = document.getElementById("pagination-wrapper")

function createArticles() {
    for (let i = 0; i < articles.length; i++) {
        let articleContainer = document.createElement("div")
        articleContainer.setAttribute("class", "article-container")
        articleContainer.innerHTML = `<img src="https://via.placeholder.com/150" alt="Illustrative article photo" class="article-photo">
        <div class="article-wrap">
        <h5>${articles[i].title}</h5>
        <p class="article-date">Posted: ${articles[i].date}</p>
        <p class="article-content">${articles[i].body}</p>
        <button class="read-more">Read more</button>
        </div>`;
        paginationWrapper.append(articleContainer)
    }
}
createArticles();

//read more buttons
const readMoreButtons = document.getElementsByClassName("read-more")

function readMore(e) {
    let dots = document.getElementsByClassName("points")
    let moreText = document.getElementsByClassName("more-text")
    let readMoreButton = document.getElementsByClassName("read-more")
    for (let i = 0; i < readMoreButton.length; i++) {
        if (e.target === readMoreButtons[i]) {
            if (dots[i].style.display === "none") {
                dots[i].style.display = "inline";
                readMoreButton[i].innerHTML = "Read more";
                moreText[i].style.display = "none";
            } else {
                dots[i].style.display = "none";
                readMoreButton[i].innerHTML = "Read less";
                moreText[i].style.display = "inline";
            }
        }
    }
}
const articleContainer = document.getElementsByClassName("article-container")
Array.from(articleContainer).forEach(element => {
    element.onclick = function (e) {
        readMore(e)
    }
});

//pagination
let numberOfPages = Math.ceil(articles.length / 3) // 3 articles per page
let pageButtonsDiv = document.getElementById("page-buttons-container")

//choose what articles to display
function page(n) {
    let allArticles = document.getElementsByClassName("article-container")
    //set all articles to display: none
    for (let i = 0; i < allArticles.length; i++) {
        allArticles[i].setAttribute("style", "display:none")
    }
    //display articles according to the page
    for (let i = n * 3 - 3; i < n * 3; i++) {
        allArticles[i].setAttribute("style", "display: flex")
    }
    let currentPage = document.getElementById("current-page")
    currentPage.innerText = `Page ${n} of ${numberOfPages}`

}
function paginate() {
    //create buttons
    for (let i = 1; i <= numberOfPages; i++) {
        let pageButton = document.createElement("button")
        pageButton.innerHTML = [i]
        pageButton.setAttribute("id", `page-${i}`)
        pageButton.addEventListener("click", () => {
            page(i)
        })
        pageButtonsDiv.append(pageButton)
    }
    
}
paginate(articles, 3);
page(1);


//search function
const searchBar = document.getElementById("search-bar")
const searchButton = document.getElementById("search-btn")

let unhighlight = () => {
    let regex = new RegExp("</>mark>|<mark>", "ig")
    let text = document.getElementsByClassName("text");
    for (let i = 0; i < text.length; i++) {
        for (let i = 0; i < text.length; i++) {
            console.log(text[i])
            let newText = text[i].innerHTML.replace(regex, "");
            text[i].innerHTML = newText;
        }

    }
}
let highlightSearch = () => {
    unhighlight()
    let searched = searchBar.value.trim()
    let text = document.getElementsByClassName("text");
    console.log(text)
    if (searched !== "") {
        for (let i = 0; i < text.length; i++) {
            console.log(text[i])
            let re = new RegExp(searched, "gi");
            let newText = text[i].innerHTML.replace(re, `<mark>${searched}</mark>`);
            text[i].innerHTML = newText;
        }
    }
}
let context = document.getElementsByClassName("context")
searchBar.addEventListener("search", () => highlightSearch())
searchButton.addEventListener("click", () => highlightSearch())


//sliders

let slides = document.getElementsByClassName("slides")
let imgIndex = Math.floor(Math.random() * slides.length);
function showSlides() {
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    imgIndex++
    if (imgIndex === slides.length) { imgIndex = 0 }
    slides[imgIndex].style.display = "block"
    console.log(imgIndex)
    setTimeout(() => showSlides(), 5000)
}
showSlides()